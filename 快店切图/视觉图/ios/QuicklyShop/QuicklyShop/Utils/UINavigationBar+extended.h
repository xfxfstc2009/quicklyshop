//
//  UINavigationBar+extended.h
//  shareCar
//
//  Created by 宋涛 on 14-2-17.
//  Copyright (c) 2014年 duonuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (extended)
- (void)setbackgroundImage:(UIImage *)image;
@end
