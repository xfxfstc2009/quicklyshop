//
//  QSTool.m
//  QuicklyShop
//
//  Created by 宋涛 on 14/6/30.
//  Copyright (c) 2014年 com. All rights reserved.
//

#import "QSTool.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation QSTool

#pragma mark - check String is Empty
+(BOOL)isEmpty:(NSString *)string
{
    if (![string isKindOfClass:[NSString class]])
        string = [string description];
    if (string == nil || string == NULL)
        return YES;
    if ([string isKindOfClass:[NSNull class]])
        return YES;
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
        return YES;
    if ([string isEqualToString:@"(null)"])
        return YES;
    if ([string isEqualToString:@"(null)(null)"])
        return YES;
    if ([string isEqualToString:@"<null>"])
        return YES;
    
    // return Default
    return NO;
}

+ (NSString *)md5:(NSString *)originalStr
{
	
	CC_MD5_CTX md5;
	CC_MD5_Init (&md5);
	CC_MD5_Update (&md5, [originalStr UTF8String], [originalStr length]);
    
	unsigned char digest[CC_MD5_DIGEST_LENGTH];
	CC_MD5_Final (digest, &md5);
	NSString *s = [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				   digest[0],  digest[1],
				   digest[2],  digest[3],
				   digest[4],  digest[5],
				   digest[6],  digest[7],
				   digest[8],  digest[9],
				   digest[10], digest[11],
				   digest[12], digest[13],
				   digest[14], digest[15]];
    // return s
	return s;
}

#pragma mark -get the available image
+ (UIImage *)getImageFromResouceName:(NSString *)aName
{
    // componentsSeparatedByString .
    NSArray  *imageArr  = [aName componentsSeparatedByString:@"."];
    // apply  a via to accept imageName and imageType
    NSString *imageName = @""; NSString *imageType = @"";
    // deciee to enter which Type
    if ([imageArr count] == 2)
    {
        imageName = [imageArr objectAtIndex:0];
        imageType = [imageArr objectAtIndex:1];
    }
    else if ([imageArr count] == 1)
    {
        imageName = aName;
        imageType = @"png";
    }
    
    // get out imagePath
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:imageType];
    // get out image
    UIImage  *image     = [[UIImage alloc] initWithContentsOfFile:imagePath];
    
    // return
    return image;
}

#pragma mark -customNavigationBar
+ (void)customNavigationBar:(UINavigationBar *)naviBar
{
    // set the property    of navigationBar
    [naviBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:kNavFontSize],UITextAttributeFont,[UIColor hexChangeFloat:kNavFontColor],UITextAttributeTextColor,nil]];
    // set backgroundImage of navigationBar
    [naviBar setbackgroundImage:[QSTool getImageFromResouceName:@"Image_Navi_bg_navigationBg"]];
}

#pragma mark - get the Left navigation barbutton item
+ (void)getLeftNavigationItemWithLeft:(BOOL)flag target:(id)atarget backgroundImage:(NSString *)imageName title:(NSString *)title
                                 frame:(CGRect)frame
{
    if (![atarget isKindOfClass:[UIViewController class]])
        return ;

    // init  and set customNavigationBtn
    UIButton *customNavigationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    // check validate
    if (CGRectEqualToRect(CGRectZero, frame)) frame = CGRectMake(0, 0, 34, 44);
    // set   Frame
    [customNavigationBtn setFrame:frame];
    // set   backgroundImage
    [customNavigationBtn setBackgroundImage:[QSTool getImageFromResouceName:imageName] forState:UIControlStateNormal];
    
    if (flag)
    {
        // add target
        [customNavigationBtn addTarget:atarget action:@selector(actionLeftBarButton) forControlEvents:UIControlEventTouchUpInside];
        // set Left
        [[(UIViewController *)atarget navigationItem]setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:customNavigationBtn]];
    }
    else
    {
        // add target
        [customNavigationBtn addTarget:atarget action:@selector(actionRightBarButton) forControlEvents:UIControlEventTouchUpInside];
        // set right
        [[(UIViewController *)atarget navigationItem]setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:customNavigationBtn]];
    }
}
#pragma mark 清除背景颜色
+(UITableViewCell *)cleanCellBg:(UITableViewCell *)cell
{
    // 1.1.清除标签的背景
    cell.textLabel.backgroundColor = [UIColor clearColor];
    // 1.2.设置标签高亮时的文字颜色为默认的文字颜色
    cell.textLabel.highlightedTextColor = cell.textLabel.textColor;
    
    // 1.3.设置cell的背景view
    UIImageView *bg = [[UIImageView alloc] init];
    cell.backgroundView = bg;
    
    UIImageView *selectedBg = [[UIImageView alloc] init];
    cell.selectedBackgroundView = selectedBg;
    return cell;
}

#pragma mark 设置Cell背景
+(UITableViewCell *)getCellImageCell:(UITableViewCell *)cell AndData:(NSArray *)data AndIndexSection:(int)indexSection AndIndexRow:(int)indexRow
{
    // 3.设置背景
    // 3.1.取出背景view
    UIImageView *bg = (UIImageView *)cell.backgroundView;
    UIImageView *selectedBg = (UIImageView *)cell.selectedBackgroundView;
    int count;
     NSString *name = nil;
    // 3.2.算出文件名
    if(indexSection==0)
    {
        count =data.count;
    }else
    {
    NSArray *sectionArray = data[indexSection];
    count =sectionArray.count;
    }
    if (count == 1)
    {   // 只有1个
        name = @"common_card_background.png";
    }
    else if (indexRow == 0)
    {   // 顶部
        name = @"common_card_top_background.png";
    }
    else if (indexRow == count - 1)
    {   // 底部
        name = @"common_card_bottom_background.png";
    }
    else
    {   // 中间
        name = @"common_card_middle_background.png";
    }
    
    
    cell.backgroundColor = kGlobalBg;
    // 3.3.设置背景图片
    bg.image = [UIImage stretchImageWithName:name];
    selectedBg.image = [UIImage stretchImageWithName:[name filenameAppend:@"_highlighted"]];
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_icon_arrow.png"]];
    cell.accessoryType=UITableViewCellAccessoryCheckmark;
    return cell;
    
}



@end
