//
//  QSTool.h
//  QuicklyShop
//
//  Created by 宋涛 on 14/6/30.
//  Copyright (c) 2014年 com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

// import Extended
#import "UINavigationBar+extended.h"
// UI Defines
#import "QuicklyShopUIDefine.h"
// UI Resouces Defines
#import "QuicklyShopImageResource.h"

@interface QSTool : NSObject

// check Empty
+(BOOL)isEmpty:(NSString *)string;
// MD5
+ (NSString *)md5:(NSString *)originalStr;
// get the available image
+ (UIImage *)getImageFromResouceName:(NSString *)aName;
// customNavigationBar
+ (void)customNavigationBar:(UINavigationBar *)naviBar;
// set Left navigationItem
+ (void)getLeftNavigationItemWithLeft:(BOOL)flag target:(id)atarget backgroundImage:(NSString *)imageName title:(NSString *)title frame:(CGRect)frame;
// 清除cell背景颜色
+(UITableViewCell *)cleanCellBg:(UITableViewCell *)cell;
// 设置Cell 背景图片
+(UITableViewCell *)getCellImageCell:(UITableViewCell *)cell AndData:(NSArray *)data AndIndexSection:(int)indexSection AndIndexRow:(int)indexRow;


@end
