//
//  UINavigationBar+extended.m
//  shareCar
//
//  Created by 宋涛 on 14-2-17.
//  Copyright (c) 2014年 duonuo. All rights reserved.
//

#import "UINavigationBar+extended.h"

#define  IOS7_OR_LATER    ([[[UIDevice currentDevice]systemVersion] compare:@"7.0"]!=NSOrderedAscending)

@implementation UINavigationBar (extended)
- (void)setbackgroundImage:(UIImage *)image
{
    if (IOS7_OR_LATER)
        [self setBackgroundImage:image forBarPosition:UIBarPositionBottom barMetrics:UIBarMetricsDefault];
    else
        [self setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
}
@end
