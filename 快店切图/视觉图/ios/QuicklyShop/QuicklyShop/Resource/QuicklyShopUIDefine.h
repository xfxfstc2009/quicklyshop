//
//  QuicklyShopUIDefine.h
//  QuicklyShop
//
//  Created by 宋涛 on 14/6/30.
//  Copyright (c) 2014年 com. All rights reserved.
//

//.h文件为定义ui property

// 字体_Property Define
//-----------导航条标题
#define kNavFontSize         17.
#define kNavFontColor        @"157dfb"
//-----------按钮
#define kBtnFontSize         17.
#define kBtnFontColor        @"FFFFFF"
//-----------表格(UITableView)
#define kTableFontSize       15.
#define kTableFontColor      @"000000"
//-----------指示性文字
#define kIndicatorFontSize   12.
#define kIndicatorFontColor  @"FFFFFF"
//-----------输入框
#define kInputFontSize       15.
#define kInputFontColor      @"000000"
//-----------输入框占位符
#define kPlaceHolderFontSize   15.
#define kPlaceHolderFontColor  @"b7b7b7"
// 字体_Property Define  Complete

// 页面_Property Define
#define kCommonBackgrounColor  @"f1f0f6"
// 页面_Property Define Complete
