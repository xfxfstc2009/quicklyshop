//
//  QuicklyShopImageResource.h
//  QuicklyShop
//
//  Created by 宋涛 on 14/6/30.
//  Copyright (c) 2014年 com. All rights reserved.
//

#ifndef QuicklyShop_QuicklyShopImageResource_h
#define QuicklyShop_QuicklyShopImageResource_h

// ------------------------------ Common
#define kQSCommon_NavBarBackImage          @"common_navBack@2x.png"


// ------------------------------ Home
#define kQSHome_TabNormalImage             @"home_TabNormalImage@2x.png"
#define kQSHome_TabHighLightImage          @"home_TabHighLightImage@2x.png"

// ------------------------------ Message
#define kQSMessage_TabNormalImage          @"message_TabNormalImage@2x.png"
#define kQSMessage_TabHighLightImage       @"message_TabHighLightImage@2x.png"

// ------------------------------ MyShop
#define kQSMyShop_TabNormalImage           @"myShop_TabNormalImage@2x.png"
#define kQSMyShop_TabHighLightImage        @"myShop_TabHighLightImage@2x.png"
#endif
