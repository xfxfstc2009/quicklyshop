//
//  GlobalKey.h
//  QuicklyShop
//
//  Created by 宋涛 on 14/6/30.
//  Copyright (c) 2014年 com. All rights reserved.
//

// 系统常用宏管理
#define USERDEFAULTS    [NSUserDefaults standardUserDefaults]
#define NOTIFICENTER    [NSNotificationCenter defaultCenter]
#define DEFAULTMANAGER  [NSFileManager defaultManager]

// 系统打印命令
#ifdef  DEBUG
#ifdef  DLog
#undef  DLog
#define DLog(fmt,...) {NSLog((@"%s [Line:%d]" fmt),__PRETTY_FUNCTION__,__LINE__,##__VA_ARGS__);}
#else
#define DLog(...)
#endif
#endif

// iPhone 5判定
#define iPhone5  ([UIScreen instancesRespondToSelector:@selector(currentMode)]?CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen]currentMode].size):NO)

// IOS7 判定
#define isIOS7_Or_Later ([[[UIDevice currentDevice] systemVersion] compare:@"7.0"]!=NSOrderedAscending)

// 背景
#define kGlobalBg [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]

